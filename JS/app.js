/* Créez une classe Chrono avec :


une méthode start()


une méthode pause()


une méthode stop()


une propriété current time
} */ 

// Variables
let divCompteur = document.getElementById("compteur") 
let startButton = document.getElementById("start")
let pauseButton = document.getElementById("pause")
let stopButton = document.getElementById("stop")
// Fonction chronometre 

function Chrono(){
  this.compteur = 0;
  this.intervalId;
  // Fonction start 
  this.start = function(){
    let self = this
    this.intervalId = setInterval(function(){
      divCompteur.innerHTML = self.compteur++
      console.log(self.compteur)
    }, 1000)
  }
  // Fonction pause 

  this.pause = function(){
    clearInterval(this.intervalId);
  }
  // Fonction stop 
  
  this.stop = function(){
    this.pause();
    this.compteur = 0;
    divCompteur.innerHTML = this.compteur;
  }
}

let chrono = new Chrono()

divCompteur.innerHTML = chrono.compteur;
startButton.addEventListener('click', function(){
  chrono.start()
})

pauseButton.addEventListener('click', function(){
  chrono.pause();
})

stopButton.addEventListener('click', function(){
  chrono.stop();
})

 

